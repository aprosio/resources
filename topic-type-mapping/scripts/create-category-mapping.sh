#! /bin/tcsh
	if ($#argv != 2) then
		echo "usage: create-config.sh"
		exit(0)
	endif

	set f = $argv[1]
	set l = $argv[2]
	
#find: (^[^=#\t]+)=([^=#]+)\r
#replace: set cat = `cat ${f} | grep -P "^Category:\1\t" | grep -Po "${l}:[^:]+:[^\t]+"`\recho ${cat}=\1\r 
#sed 's/\([^:]*\):\([^:]*\)/\2/'
set cat = `cat ${f} | grep -P "^Category:Arts	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Arts
 #Belief
set cat = `cat ${f} | grep -P "^Category:Business	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Business
 #Chronology
#Concepts
#Culture
set cat = `cat ${f} | grep -P "^Category:Gastronomy	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Gastronomy
 set cat = `cat ${f} | grep -P "^Category:Nutrition	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Nutrition
 set cat = `cat ${f} | grep -P "^Category:Travel	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Travel
 #Tourism
set cat = `cat ${f} | grep -P "^Category:Traditions	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Traditions
 set cat = `cat ${f} | grep -P "^Category:Education	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Education
 #Teaching
set cat = `cat ${f} | grep -P "^Category:Environment	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Environment
 #Geography
set cat = `cat ${f} | grep -P "^Category:Health	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Health
 set cat = `cat ${f} | grep -P "^Category:History	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=History
 #Humanities
#Comedy
#Drama
set cat = `cat ${f} | grep -P "^Category:Entertainment	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Entertainment
 #Linguistics
#Literature
set cat = `cat ${f} | grep -P "^Category:Writing	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Writing
 #Fiction
#Philosophy=Philosophy
#Language
#
set cat = `cat ${f} | grep -P "^Category:Justice	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Justice
 #Law=Law
#Life
set cat = `cat ${f} | grep -P "^Category:Medicine	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Medicine
 #Nature
#People
#Biography‎
#Fictional_characters
set cat = `cat ${f} | grep -P "^Category:Politics	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Politics
 set cat = `cat ${f} | grep -P "^Category:Science	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Science
 #Mathematics
#Society
set cat = `cat ${f} | grep -P "^Category:Economics	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Economics
 set cat = `cat ${f} | grep -P "^Category:Family	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Family
 set cat = `cat ${f} | grep -P "^Category:Military	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Military
 #Organizations
set cat = `cat ${f} | grep -P "^Category:Religion	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Religion
 set cat = `cat ${f} | grep -P "^Category:Sports	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Sports
 set cat = `cat ${f} | grep -P "^Category:Technology	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Technology
 set cat = `cat ${f} | grep -P "^Category:Africa	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Africa
 set cat = `cat ${f} | grep -P "^Category:Europe	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Europe
 set cat = `cat ${f} | grep -P "^Category:Asia	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Asia
 set cat = `cat ${f} | grep -P "^Category:South_America	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=South_America
 set cat = `cat ${f} | grep -P "^Category:Australia	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Australia
 set cat = `cat ${f} | grep -P "^Category:North_America	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=North_America
 set cat = `cat ${f} | grep -P "^Category:Oceania	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Oceania
 set cat = `cat ${f} | grep -P "^Category:Middle_East	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Middle_East
 set cat = `cat ${f} | grep -P "^Category:Communication	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Communication
 #Mass_media
set cat = `cat ${f} | grep -P "^Category:Exhibitions	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Exhibitions
 set cat = `cat ${f} | grep -P "^Category:Recreation	" | grep -Po "${l}:[^:]+:[^	]+"`
echo ${cat}=Recreation
 